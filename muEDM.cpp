#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "EDMDetectorConstruction.hh"
#include "EDMPhysicsList.hh"
#include "EDMActionInitialization.hh"
#include "EDMRunAction.hh"
#include "EDMEventAction.hh"
#include "EDMSteppingAction.hh"
#include "EDMPrimaryGeneratorAction.hh"
#include "EDMHistoManager.hh"

//ifdef G4VIS_USE
#include "G4VisExecutive.hh"
//#endif

#include "G4UIExecutive.hh"

#include <getopt.h>

int main(int argc,char** argv)
{

  G4String quencher = "isobutane";
  G4double fraction = 10.;
  G4double pressure = 1.;

  G4String macro = "run.mac";
  G4String outfile = "edm.root";

  int c;
  
  while (1) {

    static struct option long_options[] =
      {
	/* These options set a flag. */
	{"quencher", required_argument, 0, 'q'},
	{"fraction", required_argument, 0, 'f'},
	{"pressure", required_argument, 0, 'p'},
	{0, 0, 0, 0}
	
      };
    
    int option_index = 0;
    c = getopt_long (argc, argv, "q:f:p:i:o:",
		     long_options, &option_index);
    
    if (c == -1)
      break;
    
    switch (c){

    case 0:
      break;
      
    case 'q':
      quencher = G4String(optarg);
      break;

    case 'f':
      fraction = atof(optarg);
      break;

    case 'p':
      pressure = atof(optarg);
      break;

    case 'i':
      macro = G4String(optarg);
      break;

    case 'o':
      outfile = G4String(optarg);
      break;

    default:
      abort();
      
    }
    
  }

  
  EDMHistoManager*  histo = new EDMHistoManager(outfile);
  
  // construct the default run manager
  G4RunManager* runManager = new G4RunManager;

  // set mandatory initialization classes
  runManager->SetUserInitialization(new EDMDetectorConstruction);
  runManager->SetUserInitialization(new EDMPhysicsList);  
  runManager->SetUserInitialization(new EDMActionInitialization);

  EDMEventAction *EventAction = new EDMEventAction(histo);
  runManager->SetUserAction(EventAction);
  
  runManager->SetUserAction(new EDMRunAction(histo));
  runManager->SetUserAction(new EDMSteppingAction);

  runManager->SetUserAction(new EDMPrimaryGeneratorAction(EventAction));

  // initialize G4 kernel
  //runManager->Initialize();

  //#ifdef G4VIS_USE
  // Initialize visualization
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();
  //#endif
    
  G4UImanager* UI = G4UImanager::GetUIpointer();
  G4UIExecutive* ui = new G4UIExecutive(argc, argv);

  /////////Gas settings
  UI->ApplyCommand(Form("/EDM/det/quencher %s",quencher.c_str()));
  UI->ApplyCommand(Form("/EDM/det/fraction %f",fraction));
  UI->ApplyCommand(Form("/EDM/det/pressure %f bar",pressure));
  
  G4String command = "/control/execute ";
  UI->ApplyCommand(command+macro);
  //ui->SessionStart();
  
  //#ifdef G4VIS_USE
  delete visManager;
  //#endif

  // job termination
  delete runManager;
  return 0;

}
