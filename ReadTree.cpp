#include "TClonesArray.h"
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TKey.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TAxis.h"
#include "TH2F.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TROOT.h"
#include "TStyle.h"

#include "Fitter.hh"
#include "TGeoMyMagField.hh"

#include "include/EDMTPCHit.hh"
#include "include/EDMKine.hh"

#include <getopt.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char * argv[]){

  ///////////////////////////////////////////////////////////  
  //
  // Usage: ReadTree [-d] [--draw] [--verbose] -i <filename>
  //
  ///////////////////////////////////////////////////////////
  
  gROOT->LoadMacro("MyStyle.C");
  gROOT->ProcessLine("MyStyle()");

  Bool_t draw = false;
  Int_t verbose = false;
  Int_t nmax = -1;
  
  int c;

  TString filename = "in.root", outfile = "reso.dat";

  G4String quencher = "isobutane";
  G4double fraction = 10;
  G4double pressure = 1.;
  
  while (1) {

    static struct option long_options[] =
      {
	/* These options set a flag. */
	{"verbose", no_argument, 0, 'v'},
	{"draw", no_argument, 0, 'd'},
	{"quencher", required_argument, 0, 'q'},
	{"fraction", required_argument, 0, 'f'},
	{"pressure", required_argument, 0, 'p'},
	{0, 0, 0, 0}
	
      };
    
    int option_index = 0;
    c = getopt_long (argc, argv, "vdq:f:p:i:n:o:",
		     long_options, &option_index);
    
    if (c == -1)
      break;
    
    switch (c){

    case 0:
      break;
      
    case 'v':
      verbose = true;
      break;

    case 'd':
      draw = true;
      break;

    case 'q':
      quencher = optarg;
      break;

    case 'f':
      fraction = atof(optarg);
      break;

    case 'p':
      pressure = atof(optarg);
      break;

    case 'i':
      filename = TString(optarg);
      break;

    case 'n':
      nmax = atoi(TString(optarg));
      break;

    case 'o':
      outfile = TString(optarg);
      break;

    default:
      abort();
      
    }
    
  }

  if(filename == "") {

    cout << "Usage: ReadTree [-d] [--draw] -i <filename>" << endl;
    return 0;
    
  }

  Double_t Px = -22.99220;
  Double_t Py = 0.;
  Double_t Pz = -15.97999;
  ////////////////////////
  
  TVector3 norm(Px,Py,Pz); 
  TVector3 vaxis = TVector3(0.,1.,0.);
  TVector3 waxis = norm.Unit();
  TVector3 uaxis = vaxis.Cross(waxis);
  TVector3 point(1.3814699,37.674701,259.60000);

  ifstream diffusion("/Users/francesco/muonEDM/Simulation/Garfield/gas_tables/gas_database.dat");

  Double_t vdrift = -1., DL = -1., DT = -1., E = -1., Nclu = -1., Ne = -1.;
  TString tmpq;
  Double_t tmpf, tmpp;
  
  while(1){

    diffusion >> tmpq >> tmpf >> tmpp >> E >> vdrift >> DL >> DT >> Nclu >> Ne;

    if(!diffusion.good()){

      E = -1.;
      vdrift = -1.;
      DL = -1.;
      DT = -1.;
      Nclu = -1.;
      Ne = -1.;
      break;
      
    }

    cout << tmpq << " " << quencher  << " " << tmpf << " " << fraction  << " " << tmpp << " " << pressure << endl;
    if(tmpq==quencher.c_str() && tmpf==fraction && tmpp==pressure) break;
    
  }

  vdrift *= 10.;
  cout << vdrift << endl;
  
  Double_t pixel_size = 0.050;
  Double_t chip_fiducial = 12;
  Double_t anode_radius = 28.;
  Double_t chip_radius = sqrt(anode_radius*anode_radius-chip_fiducial*chip_fiducial);

  Double_t DeltaPhi = TMath::Pi()/3.;
  Double_t deltaPhi = asin((chip_fiducial/2.)/anode_radius);
  Double_t Phi0 = -TMath::Pi() + TMath::Pi()/2.;
  Double_t zoffset[4] = {200.,187.,176.,167.};

  Int_t binsize = 10.;
  const Int_t nbins = (const Int_t)chip_fiducial/pixel_size/binsize;

  if(verbose) cout << "File name: " << filename << endl;
  
  TApplication app("app", &argc, argv);

  TSystem ts;
  gSystem->Load("libEDMClassesDict");

  TFile *f = new TFile(filename);
  TTree *truth = (TTree*)f->Get("truth");

  vector<EDMTPCHit*> *TPCHits = new vector<EDMTPCHit*>;
  truth->SetBranchAddress("tpchits",&TPCHits);
  EDMKine *kine = new EDMKine();
  truth->SetBranchAddress("kine",&kine);  

  TH2F *hU = new TH2F("hU","hU;u [mm];u' [mrad]",100,-15.,15.,100,-30.,30.);
  TH2F *hV = new TH2F("hV","hV;v [mm];v' [mrad]",100,-15.,15.,100,-30.,30.);

  TH1F *hUreso = new TH1F("hUreso","hUreso; x - x_{true} [mm]; Entries [1/0.3 mm]",100,-15.,15.);
  TH1F *hVreso = new TH1F("hVreso","hVreso; y - y_{true} [mm]; Entries [1/0.3 mm]",100,-15.,15.);

  TH1F *hUPreso = new TH1F("hUPreso","hUPreso; x' - x'_{true} [mrad]; Entries [1/0.6 mrad]",100,-30.,30.);
  TH1F *hVPreso = new TH1F("hVPreso","hVPreso; y' - y'_{true} [mrad]; Entries [1/0.6 mrad]",100,-30.,30.);

  TH2F *hUsel = new TH2F("hUsel","hUsel;u [mm];u' [mrad]",100,-15.,15.,100,-30.,30.);
  TH2F *hVsel = new TH2F("hVsel","hVsel;v [mm];v' [mrad]",100,-15.,15.,100,-30.,30.);

  TH1F *hThetaReso = new TH1F("hThetaReso","hThetaReso",100,-30.,30.);
  TH2F *hThetaMom = new TH2F("hThetaMom","hThetaMom",100.,-0.5,0.5,100,-30.,30.);

  TH1F *hMomFit = new TH1F("hMomFit","p_{#mu};p_{#mu} [MeV/c]; Entries [1/(0.1 MeV/c)]",100,25.,30.);
  TH1F *hMomFirst = new TH1F("hMomFirst","p^{0}_{#mu};p_{#mu} [MeV/c]; Entries [1/(0.1 MeV/c)]",100,25.,30.);
  TH1F *hMomFirstTrue = new TH1F("hMomFirstTrue","p^{0,true}_{#mu};p_{#mu} [MeV/c]; Entries [1/(0.1 MeV/c)]",100,25.,30.);

  TH2F *hOcc = new TH2F("hOcc","hOcc",100,150.,210.,100,-3.5,3.5);
  TH2F *hOccPixel = new TH2F("hOccPixec","hOccPixel",1101,-0.5,1100.5,251,-0.5,250.5);

  hUsel->SetLineColor(kRed);
  hVsel->SetLineColor(kRed);
  
  Fitter fitter("geom.C",Form("\"%s\",%f,%f",quencher.c_str(),fraction,pressure),new TGeoMyMagField("BENfield_map.dat"),verbose);

  Int_t Nev = (nmax > 0) ? TMath::Min(nmax,(Int_t)truth->GetEntries()) : truth->GetEntries();
  
  for(Int_t iev=0;iev<Nev;iev++){

    truth->GetEntry(iev);

    if(iev%100 == 0) cout << iev << "\r" << flush;

    Int_t nTPChits = TPCHits->size();
    if(nTPChits<3) continue;
   
    TGraph *grXY = new TGraph();
    TGraph *grXYfit = new TGraph();
    TGraph *grZPhi = new TGraph();
    TGraph *grZPhifit = new TGraph();

    grXY->SetMarkerStyle(7);
    grXYfit->SetMarkerStyle(20);
    grXYfit->SetMarkerColor(kRed);
    grZPhi->SetMarkerStyle(7);
    grZPhifit->SetMarkerStyle(7);
    grZPhifit->SetMarkerColor(kRed);

    hU->Fill(kine->u,kine->uprime*1.e3);
    hV->Fill(kine->v,kine->vprime*1.e3);

    TVector3 first_mom((*TPCHits)[0]->GetMom().x(),(*TPCHits)[0]->GetMom().y(),(*TPCHits)[0]->GetMom().z());
    
    hMomFirstTrue->Fill(first_mom.Mag());

    Double_t dt = gRandom->Gaus(0.,5.);
    
    /*
    if(nTPChits > 250){
	
      hUsel->Fill(kine->u,kine->uprime*1.e3);
      hVsel->Fill(kine->v,kine->vprime*1.e3);
      
    }
    */

    Double_t phi[4][nbins];
    Double_t r[4][nbins];
    Double_t z[4][nbins];
    Int_t nhit[4][nbins];

    for(Int_t ich=0;ich<4;ich++){
      for(Int_t ih=0;ih<nbins;ih++){
	phi[ich][ih] = 0.;
	r[ich][ih] = 0.;
	z[ich][ih] = 0.;
	nhit[ich][ih] = 0;
      }
    }
    
    for(Int_t ih=0;ih<nTPChits;ih++){

      EDMTPCHit *aHit = (*TPCHits)[ih];
      Int_t px = aHit->GetPixelX();
      Int_t py = aHit->GetPixelY();
      Int_t chip = aHit->GetChip();
      Double_t phi_hit = Phi0 + chip*DeltaPhi + asin((px - chip_fiducial/pixel_size/2. + 0.5)*pixel_size/anode_radius);
      Double_t z_hit = zoffset[chip] + (py - chip_fiducial/pixel_size/2. + 0.5)*pixel_size;
      Double_t r_hit = chip_radius/cos(phi_hit - (Phi0 + chip*DeltaPhi)) + vdrift*(aHit->GetTime()+dt);
      hOccPixel->Fill(250*chip+px,py);
      
      phi[chip][py/binsize] += phi_hit;
      z[chip][py/binsize] += z_hit;
      r[chip][py/binsize] += r_hit;
      nhit[chip][py/binsize]++;

    }

    Int_t nhit_tot = 0;

    vector<TVector3> hits;
    vector<Int_t> hitsize;
    
    for(Int_t ich=0;ich<4;ich++){

      for(Int_t ih=0;ih<nbins;ih++){
	
	if(nhit[ich][ih] == 0) continue;

	phi[ich][ih] /= nhit[ich][ih];
	z[ich][ih] /= nhit[ich][ih];
	r[ich][ih] /= nhit[ich][ih];

	G4ThreeVector pos(r[ich][ih]*cos(phi[ich][ih]),r[ich][ih]*sin(phi[ich][ih]),z[ich][ih]);
	
	hits.push_back(TVector3(pos.x(),pos.y(),pos.z()));
	hitsize.push_back(nhit[ich][ih]);
	nhit_tot++;
	
	grXY->SetPoint(grXY->GetN(),pos.x(),pos.y());
	grZPhi->SetPoint(grZPhi->GetN(),pos.z(),pos.phi());
	
	hOcc->Fill(pos.z(),pos.phi());

      }

    }

    if(nhit_tot == 0) continue;
    
    TMatrixDSym Cov0(6);
    Cov0[0][0] = Cov0[1][1] = Cov0[2][2] = pow(2., 2.);
    Cov0[3][3] = 0.2;
    Cov0[4][4] = Cov0[5][5] = pow(0.2, 2.);

    TVector3 mom0;
    Int_t i1 = 10;
    while(mom0.Mag() == 0){
      mom0 = 28.0*(hits[i1]-hits[0]).Unit();
      i1++;
      if(mom0.Z() > 0) mom0 *= -1.;
    }

    //mom0 = TVector3(25.083639,3.193563,-12.025481);

    /////////// FITTER /////////
    fitter.InitializeTrack(hits[0],mom0,Cov0);

    for(Int_t ih=0;ih<nhit_tot;ih++){
      
      TVectorD hitCoords(3);
      hitCoords[0] = hits[ih].X();
      hitCoords[1] = hits[ih].Y();
      hitCoords[2] = hits[ih].Z();
      
      TMatrixDSym hitCov(3);
      hitCov[0][0] = pow(8./sqrt(hitsize[ih]),2);
      hitCov[1][1] = pow(8./sqrt(hitsize[ih]),2);
      hitCov[2][2] = pow(8./sqrt(hitsize[ih]),2);

      fitter.AddHit3D(ih,hitCoords,hitCov);
      
    }

    Bool_t isConverged = false;
    try{
      isConverged = fitter.Fit();
    }
    catch (genfit::Exception &e){ isConverged = false; }
    
    //mom0.Print();
    //////////////////////////
    
    if(isConverged){

      TVector3 vtx_pos;
      TVector3 vtx_mom;
      TVector2 vtx_uv;
      TVector2 vtx_uvprime;

      //cout << "####### Propagation ########" << endl;
      fitter.PropagateToPlane(point,uaxis,vaxis,vtx_pos,vtx_mom,vtx_uv,vtx_uvprime);
      
      //hUsel->Fill(kine->u,kine->uprime*1.e3);
      //hVsel->Fill(kine->v,kine->vprime*1.e3);
      hUsel->Fill(vtx_uv.X(),vtx_uvprime.X()*1.e3);
      hVsel->Fill(vtx_uv.Y(),vtx_uvprime.Y()*1.e3);
      hUreso->Fill(vtx_uv.X()-kine->u);
      hVreso->Fill(vtx_uv.Y()-kine->v);
      hUPreso->Fill((vtx_uvprime.X()-kine->uprime)*1.e3);
      hVPreso->Fill((vtx_uvprime.Y()-kine->vprime)*1.e3);
      hThetaReso->Fill((vtx_mom.Theta()-TVector3(kine->xmom,kine->ymom,kine->zmom).Theta())*1.e3);
      hMomFit->Fill(vtx_mom.Mag());
      hThetaMom->Fill(vtx_mom.Mag()-28.0,(vtx_mom.Theta()-TVector3(kine->xmom,kine->ymom,kine->zmom).Theta())*1.e3);
      
    }

    for(Int_t ih=0;ih<nhit_tot;ih++){

      TVector3 fit_pos;
      TVector3 fit_mom;
      TMatrixDSym fit_Cov(6);

      Bool_t good = fitter.GetState(ih,fit_pos,fit_mom,fit_Cov);
      
      if(good){

	grXYfit->SetPoint(grXYfit->GetN(),fit_pos.x(),fit_pos.y());
	grZPhifit->SetPoint(grZPhifit->GetN(),fit_pos.z(),fit_pos.Phi());
	
	if(ih==0 && isConverged){
	  hMomFirst->Fill(fit_mom.Mag());
	}

      }
      
    }
    
    if(draw){
      
      if(grXY->GetN()>0){
	
	TH2F *hXY = new TH2F("hXY","XY;X [mm];Y [mm]",100,-50.,50.,100,-50.,50.);
	TH2F *hZPhi = new TH2F("hZPhi","Z#phi;Z [mm];#phi [rad]",100,100.,300.,100,-3.5,3.5);
	TCanvas *cc = new TCanvas("cc");
	cc->Divide(2,1);
	cc->cd(1);
	hXY->Draw();
	grXYfit->Draw("Psame");
	grXY->Draw("Psame");
	cc->cd(2);
	hZPhi->Draw();
	grZPhifit->Draw("Psame");
	grZPhi->Draw("Psame");
	cc->Update();
	cc->WaitPrimitive();
	delete grXY;
	delete grZPhi;
	delete hXY;
	delete hZPhi;
	delete cc;

      }
      
    } 

  }

  TH1D *hUX = hU->ProjectionX();
  TH1D *hUselX = hUsel->ProjectionX();
  hUselX->Scale(hUX->GetMaximum()/hUselX->GetMaximum());

  TH1D *hVX = hV->ProjectionX();
  TH1D *hVselX = hVsel->ProjectionX();
  hVselX->Scale(hVX->GetMaximum()/hVselX->GetMaximum());

  TH1D *hUY = hU->ProjectionY();
  TH1D *hUselY = hUsel->ProjectionY();
  hUselY->Scale(hUY->GetMaximum()/hUselY->GetMaximum());

  TH1D *hVY = hV->ProjectionY();
  TH1D *hVselY = hVsel->ProjectionY();
  hVselY->Scale(hVY->GetMaximum()/hVselY->GetMaximum());

  TCanvas *c1 = new TCanvas("c1");
  c1->Divide(4,2);
  c1->cd(1);
  hU->Draw("colz");
  c1->cd(2);
  hUX->Draw();
  hUselX->Draw("same hist");
  c1->cd(3);
  hV->Draw("colz");
  c1->cd(4);
  hVX->Draw();
  hVselX->Draw("same hist");
  c1->cd(5);
  hUsel->Draw("colz");
  c1->cd(6);
  hUY->Draw();
  hUselY->Draw("same hist");
  c1->cd(7);
  hVsel->Draw("colz");
  c1->cd(8);
  hVY->Draw();
  hVselY->Draw("same hist");
  c1->Update();

  TCanvas *c10 = new TCanvas("c10");
  c10->Divide(2,2);
  c10->cd(1);
  hUreso->Fit("gaus");
  c10->cd(2);
  hVreso->Fit("gaus");
  c10->cd(3);
  hUPreso->Fit("gaus");
  c10->cd(4);
  hVPreso->Fit("gaus");
  c10->Update();

  TCanvas *c2 = new TCanvas("c2");
  hOccPixel->Draw("colz");
  c2->Update();

  TCanvas *c3 = new TCanvas("c3");
  hOcc->Draw("colz");
  c3->Update();

  TCanvas *c4 = new TCanvas("c4");
  //gStyle->SetOptFit(1);
  c4->Divide(3,1);
  c4->cd(1);
  hMomFit->Fit("gaus");
  c4->cd(2);
  hMomFirst->Draw();
  c4->cd(3);
  hMomFirstTrue->Draw();
  c4->Update();

  TCanvas *c5 = new TCanvas("c5");
  c5->Divide(2,1);
  c5->cd(1);
  hThetaReso->Fit("gaus");
  c5->cd(2);
  hThetaMom->Draw("colz");
  c5->Update();

  ofstream fout(outfile,std::ios_base::app);

  fout << quencher << "\t" << fraction << "\t" << pressure << "\t" << E << "\t" 
       << hMomFit->GetFunction("gaus")->GetParameter(2) << "\t" 
       << hUPreso->GetFunction("gaus")->GetParameter(2) << "\t" 
       << hVPreso->GetFunction("gaus")->GetParameter(2) << "\t" 
       << hThetaReso->GetFunction("gaus")->GetParameter(2) 
       << endl;
    
  app.Run(true);
  
  return 1;
  
}
