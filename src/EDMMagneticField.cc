//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EDMMagneticField.cc
/// \brief Implementation of the EDMMagneticField class

#include "EDMMagneticField.hh"

#include "G4GenericMessenger.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"

#include "Riostream.h"
#include "TH2F.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMMagneticField::EDMMagneticField()
: G4MagneticField(), 
  fMessenger(nullptr)
{
  // define commands for this class
  DefineCommands();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMMagneticField::~EDMMagneticField()
{ 
  delete fMessenger; 
}

void EDMMagneticField::GetFieldValue(const G4double point[4], double *bField) const
{

  G4double R = sqrt(point[0]*point[0]+point[1]*point[1]);
  G4double phi = atan2(point[1],point[0]);
  G4double Z = point[2];
  
  G4double Br = fBr->GetBinContent(fBr->FindBin(R,fabs(Z)));
  G4double Bz = fBz->GetBinContent(fBz->FindBin(R,fabs(Z)));

  if(Z < 0) Br *= -1.;

  bField[0] = Br*cos(phi)*tesla;
  bField[1] = Br*sin(phi)*tesla;
  bField[2] = Bz*tesla;
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void EDMMagneticField::ReadField(){

  std::ifstream infile("BENfield_map.dat");

  G4double Z0, dR, dZ;
  G4int nR, nZ;
  infile >> Z0 >> nR >> nZ >> dR >> dZ;

  fBr = new TH2F("hBr","hBr",nR-1,-0.5*dR,(nR-1.5)*dR,nZ-1,-0.5*dZ+Z0,(nZ-1.5)*dZ+Z0);
  fBz = new TH2F("hBz","hBz",nR-1,-0.5*dR,(nR-1.5)*dR,nZ-1,-0.5*dZ+Z0,(nZ-1.5)*dZ+Z0);
  
  G4double R, Z, Br, Bz;
  
  for(G4int ir=0;ir<nR;ir++){
    for(G4int iz=0;iz<nZ;iz++){

      infile >> R >> Z >> Br >> Bz;
      fBr->SetBinContent(fBr->FindBin(R,Z),Br);
      fBz->SetBinContent(fBz->FindBin(R,Z),Bz);
      
    }
  }

}

void EDMMagneticField::DefineCommands()
{
  // Define /EDM/field command directory using generic messenger class
  fMessenger = new G4GenericMessenger(this, 
                                      "/EDM/field/", 
                                      "Field control");
  /*
  // fieldValue command 
  auto& valueCmd
    = fMessenger->DeclareMethodWithUnit("value","tesla",
                                &EDMMagneticField::SetField, 
                                "Set field strength.");
  valueCmd.SetParameterName("field", true);
  valueCmd.SetDefaultValue("1.");
  */
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
