#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <CLHEP/Units/SystemOfUnits.h>
#include "G4PhysicalConstants.hh"

#include "EDMHistoManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

using namespace std;

EDMHistoManager::EDMHistoManager(G4String outfile)
:fRootFile(0),
 fNtuple(0)
{

  // histograms
  for (G4int k=0; k<MaxHisto; k++) fHisto[k] = 0;
    
  // ntuple
  fNtuple = 0;

  fOutfile = outfile;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMHistoManager::~EDMHistoManager()
{
  if ( fRootFile ) delete fRootFile;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::book()
{ 
 // Creating a tree container to handle histograms and ntuples.
 // This tree is associated to an output file.
 //
 fRootFile = new TFile(fOutfile,"RECREATE");
 if(!fRootFile) {
   G4cout << " EDMHistoManager::book :" 
          << " problem creating the ROOT TFile "
          << G4endl;
   return;
 }
 
 //create ntuple
 fNtuple = new TTree("truth", "MC Truth");
 fNtuple->Branch("Event", &fEvent, "Event/I");
 fNtuple->Branch("tpchits", &fTPCHitArr,32000,-1);
 fNtuple->Branch("kine", &fKine,32000,99);
 
 G4cout << "\n----> Histogram file is opened in " << fOutfile << G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::save()
{ 
  if (fRootFile) {
    fRootFile->Write();       // Writing the histograms to the file
    fRootFile->Close();        // and closing the tree (and the file)
    G4cout << "\n----> Histogram Tree is saved \n" << G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::FillHisto(G4int ih, G4double xbin, G4double weight)
{
  if (ih >= MaxHisto) {
    G4cout << "---> warning from EDMHistoManager::FillHisto() : histo " << ih
           << " does not exist. (xbin=" << xbin << " weight=" << weight << ")"
           << G4endl;
    return;
  }
 if  (fHisto[ih]) { fHisto[ih]->Fill(xbin, weight); }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::Normalize(G4int ih, G4double fac)
{
  if (ih >= MaxHisto) {
    G4cout << "---> warning from EDMHistoManager::Normalize() : histo " << ih
           << " does not exist. (fac=" << fac << ")" << G4endl;
    return;
  }
  if (fHisto[ih]) fHisto[ih]->Scale(fac);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::FillNtuple(G4int event, vector<EDMTPCHit*> *TPCHitArr, EDMKine kine)
{
  
  fEvent = event;
  fTPCHitArr = *TPCHitArr;
  fKine = kine;

  if (fNtuple) fNtuple->Fill();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMHistoManager::PrintStatistic()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


