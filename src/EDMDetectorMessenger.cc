#include "EDMDetectorMessenger.hh"

#include "EDMDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMDetectorMessenger::EDMDetectorMessenger(EDMDetectorConstruction* Det)
: G4UImessenger(),
  fDetector(Det),
  fEDMDir(0),
  fDetDir(0)
{ 

  fEDMDir = new G4UIdirectory("/EDM/");
  fEDMDir->SetGuidance("UI commands of EDM");
  
  G4bool broadcast = false;
  fDetDir = new G4UIdirectory("/EDM/det/",broadcast);
  fDetDir->SetGuidance("detector control");

  fQuencherCmd = new G4UIcmdWithAString("/EDM/det/quencher",this);
  fQuencherCmd->SetGuidance("isobutane, co2 or cf4");
  fQuencherCmd->SetParameterName("quencher",false);
  fQuencherCmd->SetDefaultValue("isobutane");
  fQuencherCmd->AvailableForStates(G4State_PreInit);

  fFractionCmd = new G4UIcmdWithADouble("/EDM/det/fraction",this);
  fFractionCmd->SetGuidance("TPC quencher fraction in %s");
  fFractionCmd->SetParameterName("fraction",false);
  fFractionCmd->SetRange("fraction>=0.");
  fFractionCmd->SetDefaultValue(0.1);
  fFractionCmd->AvailableForStates(G4State_PreInit);

  fPressureCmd = new G4UIcmdWithADoubleAndUnit("/EDM/det/pressure",this);
  fPressureCmd->SetGuidance("TPC gas pressure");
  fPressureCmd->SetParameterName("pressure",false);
  fPressureCmd->SetRange("pressure>=0.");
  fPressureCmd->SetUnitCategory("Pressure");
  fPressureCmd->SetDefaultUnit("bar");
  fPressureCmd->SetDefaultValue(1.);
  fPressureCmd->AvailableForStates(G4State_PreInit);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMDetectorMessenger::~EDMDetectorMessenger()
{

  delete fQuencherCmd;
  delete fFractionCmd;
  delete fPressureCmd;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  
  if( command == fQuencherCmd )
    { fDetector->SetQuencher(newValue); }
  if( command == fFractionCmd )
    { fDetector->SetFraction(fFractionCmd->GetNewDoubleValue(newValue)); }
  if( command == fPressureCmd )
    { fDetector->SetPressure(fPressureCmd->GetNewDoubleValue(newValue)); }
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
