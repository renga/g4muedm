#include "EDMPrimaryGeneratorAction.hh"
#include "EDMEventAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4RotationMatrix.hh"
#include "Randomize.hh"

#include "TVectorD.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"

using namespace CLHEP;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMPrimaryGeneratorAction::EDMPrimaryGeneratorAction(EDMEventAction* EventAction)
  : fMphaseU(2,2),
    fSphaseU(2,2),
    fMphaseV(2,2),
    fSphaseV(2,2)
{

  G4int n_particle = 1;
  particleGun  = new G4ParticleGun(n_particle);

  fEventAction = EventAction;

  //TO BE PUT IN MESSENGER
  G4double eps_u = 17.9*mm*mrad;
  G4double alpha_u = -1.;
  G4double beta_u = 0.8*mm/mrad;
  G4double gamma_u = 2.5*mrad/mm;

  G4double eps_v = 38.2*mm*mrad;
  G4double alpha_v = -0.4;
  G4double beta_v = 0.4*mm/mrad;
  G4double gamma_v = 3.3*mrad/mm;
  /////////////

  G4double sigma_u = sqrt(eps_u*beta_u);
  G4double sigma_up = sqrt(eps_u*gamma_u);
  G4double rho_u = -alpha_u*sqrt(1./beta_u/gamma_u);

  G4double sigma_v = sqrt(eps_v*beta_v);
  G4double sigma_vp = sqrt(eps_v*gamma_v);
  G4double rho_v = -alpha_v*sqrt(1./beta_v/gamma_v);

  G4cout << "######## Phase space ########" << G4endl;
  G4cout << "     \tsigma_u\tsigma_u'\trho_ux'" << G4endl;
  G4cout << "--------------------------------------------" << G4endl;
  G4cout << "x  | \t" << sigma_u << " \t" << sigma_up << "\t" << rho_u << G4endl;
  G4cout << "y  | \t" << sigma_v << " \t" << sigma_vp << "\t" << rho_v << G4endl;
  G4cout << "--------------------------------------------" << G4endl;

  TMatrixDSym CovU(2);
  CovU[0][0] = sigma_u*sigma_u;
  CovU[0][1] = CovU[1][0] = rho_u*sigma_u*sigma_up;
  CovU[1][1] = sigma_up*sigma_up;
  
  TMatrixDSymEigen eigenU(CovU);
  TMatrixD MU = eigenU.GetEigenVectors();
  fMphaseU = MU.Invert();
  fSphaseU = TMatrixD(MU,TMatrixD::kMult,TMatrixD(CovU,TMatrixD::kMultTranspose,MU));
  
  TMatrixDSym CovV(2);
  CovV[0][0] = sigma_v*sigma_v;
  CovV[0][1] = CovV[1][0] = rho_v*sigma_v*sigma_vp;
  CovV[1][1] = sigma_vp*sigma_vp;
  
  TMatrixDSymEigen eigenV(CovV);
  TMatrixD MV = eigenV.GetEigenVectors();
  fMphaseV = MV.Invert();
  fSphaseV = TMatrixD(MV,TMatrixD::kMult,TMatrixD(CovV,TMatrixD::kMultTranspose,MV));

  //TO BE PUT IN MESSENGER
  G4double Px = -22.99220*MeV;
  G4double Py = 0.*MeV;
  G4double Pz = -15.97999*MeV;
  ////////////////////////
  
  fMom = G4ThreeVector(Px,Py,Pz);
  fVtx = G4ThreeVector(1.3814699*mm,37.674701*mm,259.60000*mm);

  fVaxis = G4ThreeVector(0.,1.,0.);
  fWaxis = fMom.unit();
  fUaxis = fVaxis.cross(fWaxis);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMPrimaryGeneratorAction::~EDMPrimaryGeneratorAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4ParticleDefinition* muon
    = G4ParticleTable::GetParticleTable()->FindParticle("mu-");
  
  double mass = muon->GetPDGMass();

  double E = sqrt(fMom.mag2() + mass*mass) - mass;

  G4ThreeVector mom;
  G4ThreeVector vtx;

  GeneratePhaseSpace(mom,vtx);
  
  G4ThreeVector dir = mom/mom.mag();

  particleGun->SetParticleDefinition(muon);
  particleGun->SetParticlePosition(vtx);
  particleGun->SetParticleMomentumDirection(dir);
  particleGun->SetParticleEnergy(E);
  particleGun->GeneratePrimaryVertex(anEvent);
  
}

void EDMPrimaryGeneratorAction::GeneratePhaseSpace(G4ThreeVector &mom, G4ThreeVector &vtx){

  //Random generation of the phase space
  TVectorD p(2);
  
  /////Horizontal
  p[0] = G4RandGauss::shoot(0.,sqrt(fSphaseU[0][0]));
  p[1] = G4RandGauss::shoot(0.,sqrt(fSphaseU[1][1]));
  
  G4double u = 0.;
  G4double uprime = 0.;

  for(int j=0;j<2;j++) u += fMphaseU[j][0]*p[j];
  for(int j=0;j<2;j++) uprime += fMphaseU[j][1]*p[j];

  /////Vertical
  p[0] = G4RandGauss::shoot(0.,sqrt(fSphaseV[0][0]));
  p[1] = G4RandGauss::shoot(0.,sqrt(fSphaseV[1][1]));
  
  G4double v = 0.;
  G4double vprime = 0.;

  for(int j=0;j<2;j++) v += fMphaseV[j][0]*p[j];
  for(int j=0;j<2;j++) vprime += fMphaseV[j][1]*p[j];

  G4double pw = fMom.mag()/sqrt(1+uprime*uprime+vprime*vprime);
  G4double pu = pw*uprime;
  G4double pv = pw*vprime;

  mom = LocalToGlobal(G4ThreeVector(pu,pv,pw));
  vtx = fVtx + LocalToGlobal(G4ThreeVector(u,v,0.));
  
  EDMKine kine;
  kine.SetPhaseSpace(u,uprime,v,vprime);
  kine.SetMomentum(mom.x(),mom.y(),mom.z());
  kine.SetVertex(vtx.x(),vtx.y(),vtx.z());
  fEventAction->SetKine(kine);
    
  
}

G4ThreeVector EDMPrimaryGeneratorAction::LocalToGlobal(G4ThreeVector uvw){
  
  TVectorD pos(3);
  pos[0] = uvw[0];
  pos[1] = uvw[1];
  pos[2] = uvw[2];
    
  TMatrixD M(3, 3);
  M[0][0] = fUaxis.x();
  M[0][1] = fVaxis.x();
  M[0][2] = fWaxis.x();
  M[1][0] = fUaxis.y();
  M[1][1] = fVaxis.y();
  M[1][2] = fWaxis.y();
  M[2][0] = fUaxis.z();
  M[2][1] = fVaxis.z();
  M[2][2] = fWaxis.z();
  
  pos = M * pos;
  
  return G4ThreeVector(pos[0], pos[1], pos[2]);
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


