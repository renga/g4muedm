//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EDMLTPCSD.cc
/// \brief Implementation of the EDMLTPCSD class

#include "EDMLTPCSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4Poisson.hh"
#include "G4ParticleTable.hh"

#include "TMath.h"

using namespace CLHEP;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMLTPCSD::EDMLTPCSD(const G4String& name,
                         const G4String& hitsCollectionName) 
 : G4VSensitiveDetector(name),
   fHitsCollection(NULL)
{
  collectionName.insert(hitsCollectionName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMLTPCSD::~EDMLTPCSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMLTPCSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection

  fHitsCollection 
    = new EDMTPCHitsCollection(SensitiveDetectorName, collectionName[0]); 

  // Add this collection in hce

  G4int hcID 
    = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hce->AddHitsCollection( hcID, fHitsCollection ); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool EDMLTPCSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  

  G4ParticleTable *ParticleTable = G4ParticleTable::GetParticleTable();

  G4Track *track = aStep->GetTrack();

  G4double lambda;
  
  G4double N_He;
  G4double N_iC4H10;
  SpecificIonizationElectron(aStep->GetPreStepPoint()->GetMomentum().mag(),N_He,N_iC4H10);

  G4double f_iC4H10 = 0.1;
  G4double f_He = 1. - f_iC4H10;

  //TO BE PUT IN MESSENGER
  G4double lambda_ele = 1.*centimeter / ((N_iC4H10 * f_iC4H10 + N_He * f_He) * 0.5); //0.5 bar
  G4double lambda_mu = 1.*centimeter/(180. * 0.5); // 0.5 bar
  G4double ave_nele = 1.2;
  //////////
  
  if(track->GetDefinition() == ParticleTable->FindParticle("e+")
     || track->GetDefinition() == ParticleTable->FindParticle("e-")) lambda = lambda_ele;
  
  else if(track->GetDefinition() == ParticleTable->FindParticle("mu+")
	  || track->GetDefinition() == ParticleTable->FindParticle("mu-")) lambda = lambda_mu;

  else return false;
  
  // energy deposit
  G4double edep = aStep->GetTotalEnergyDeposit();

  G4ThreeVector pos0 = aStep->GetPreStepPoint()->GetPosition();
  G4ThreeVector pos1 = aStep->GetPostStepPoint()->GetPosition();

  G4ThreeVector mom0 = aStep->GetPreStepPoint()->GetMomentum();
  G4ThreeVector mom1 = aStep->GetPostStepPoint()->GetMomentum();
  G4ThreeVector mom = 0.5*(mom0+mom1);
  
  G4double length = (pos1-pos0).mag();
  G4int nclu = G4Poisson(length/lambda);

  G4double vdrift = 20.*micrometer/nanosecond;
  
  G4double pixel_size = 50.*micrometer;
  G4double chip_fiducial = 12.*millimeter;
  G4double anode_radius = 28.*millimeter;
  G4double chip_radius = sqrt(anode_radius*anode_radius-chip_fiducial*chip_fiducial);
  
  G4double DeltaPhi = pi/3.;
  G4double deltaPhi = asin((chip_fiducial/2.)/anode_radius);
  G4double Phi0 = -pi + pi/2.;
  G4double zoffset[4] = {200.*millimeter,187.*millimeter,176.*millimeter,167.*millimeter};

  G4double DT = 400.*micrometer/sqrt(centimeter);
  G4double DL = 600.*micrometer/sqrt(centimeter);
  
  for(G4int ic=0;ic<nclu;ic++){

    G4int nele = G4Poisson(ave_nele);
    G4ThreeVector pos = pos0 + G4RandFlat::shoot(0.,1.)*(pos0-pos1);

    //G4cout << pos.phi() << "  " << pos.phi() - Phi0 << G4endl;

    ///Accept on a 10% wider range because some marginal hit can enter the acceptance thanks to the diffusion
    if(
       (fabs(pos.phi() - Phi0) < deltaPhi*1.1 && fabs(pos.z() - zoffset[0]) < chip_fiducial/2.*1.1) ||
       (fabs(pos.phi() - (Phi0 + DeltaPhi)) < deltaPhi*1.1 && fabs(pos.z() - zoffset[1]) < chip_fiducial/2.*1.1) ||       
       (fabs(pos.phi() - (Phi0 + 2.*DeltaPhi)) < deltaPhi*1.1 && fabs(pos.z() - zoffset[2]) < chip_fiducial/2.*1.1) ||       
       (fabs(pos.phi() - (Phi0 + 3.*DeltaPhi)) < deltaPhi*1.1 && fabs(pos.z() - zoffset[3]) < chip_fiducial/2.*1.1)
       ) {

      for(G4int ie=0;ie<nele;ie++){
	
	G4int px = -999, py = -999;
	G4int chip = -1;
	G4double time = -999.;

	for(G4int ich=0;ich<4;ich++){

	  if(fabs(pos.phi() - (Phi0 + ich*DeltaPhi)) < deltaPhi*1.1){
	    chip = ich;
	    Double_t ddrift = pos.perp() -  chip_radius/cos(pos.phi() - (Phi0 + ich*DeltaPhi));
	    Double_t chipx = anode_radius*sin(pos.phi() - (Phi0 + ich*DeltaPhi)) + G4RandGauss::shoot(0.,DT*sqrt(ddrift));
	    Double_t chipz = pos.z() - zoffset[ich] + G4RandGauss::shoot(0.,DT*sqrt(ddrift));
	    //Double_t chipx = anode_radius*sin(pos.phi() - (Phi0 + ich*DeltaPhi)) + G4RandGauss::shoot(0.,400.*micrometer);
	    //Double_t chipz = pos.z() - zoffset[ich] + G4RandGauss::shoot(0.,400.*micrometer);
	    px = TMath::Nint(chipx/pixel_size + chip_fiducial/pixel_size/2. - 0.5);
	    py = TMath::Nint(chipz/pixel_size + chip_fiducial/pixel_size/2. - 0.5);
	    time = (ddrift + G4RandGauss::shoot(0.,DL*sqrt(ddrift)))/vdrift;
	    //time = (ddrift + G4RandGauss::shoot(0.,600.*micrometer))/vdrift;
	    break;
	  }

	}

	if(px > 0 && py > 0 && px < chip_fiducial/pixel_size && py < chip_fiducial/pixel_size){

	  EDMTPCHit* newHit = new EDMTPCHit();
	
	  newHit->SetTrackID(aStep->GetTrack()->GetTrackID());
	  newHit->SetPos(pos);
	  newHit->SetMom(mom);
	  
	  newHit->SetChip(chip);
	  newHit->SetPixelX(px);
	  newHit->SetPixelY(py);
	  newHit->SetTime(time);
	  
	  fHitsCollection->insert( newHit );
	  
	}
	
      }

    }

  }

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMLTPCSD::EndOfEvent(G4HCofThisEvent*)
{
  if ( verboseLevel>1 ) { 
     G4int nofHits = fHitsCollection->entries();
     G4cout << G4endl
            << "-------->Hits Collection: in this event they are " << nofHits 
            << " hits in the tracker chambers: " << G4endl;
     for ( G4int i=0; i<nofHits; i++ ) (*fHitsCollection)[i]->Print();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMLTPCSD::SpecificIonizationElectron(G4double _pMom, G4double &nhe, G4double &niso)
{

   /////////////////////////////////////////////////////////////////////////////
   // Return the numner of electrons per unit length (helium and
   // isobutane contributions)
   // Positron case
   /////////////////////////////////////////////////////////////////////////////
  
   ////////VALUES FROM GARFIELD SIMULATIONS
  G4double pstep[10] = {0.05 * MeV, 0.1 * MeV, 0.25 * MeV, 0.4 * MeV, 0.6 * MeV, 0.8 * MeV, 0.86 * MeV, 10 * MeV, 20 * MeV, 52.8 * MeV};
  G4double NHe[10] = {253.9, 76.40, 12.8, 6.8, 4.7, 4.0, 3.9, 4.1, 4.6, 5.4};
  G4double Niso[10] = {4032.9, 1381.4, 327.8, 180.8, 124.7, 105, 102.4, 104.1, 111.6, 115.4};
  
  for (int i = 0; i < 9; i++) {
    
    //linear extrapolation below pstep[0]
    //linear interpolation above pstep[0] and below pstep[9]
    if (_pMom <= pstep[i + 1]) {
      niso = Niso[i] + (_pMom - pstep[i]) / (pstep[i + 1] - pstep[i]) * (Niso[i + 1] - Niso[i]);
      nhe = NHe[i] + (_pMom - pstep[i]) / (pstep[i + 1] - pstep[i]) * (NHe[i + 1] - NHe[i]);
      return;
    }
    
  }
  
  //if _pMom > pstep[9]
  niso = Niso[9];
  nhe = NHe[9];
  return;
  
}
