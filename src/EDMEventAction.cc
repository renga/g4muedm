#include "EDMEventAction.hh"

#include "EDMRunAction.hh"
#include "EDMHistoManager.hh"
#include "EDMTPCHit.hh"

#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4Event.hh"
#include "G4UnitsTable.hh"
#include "G4ThreeVector.hh"

#include "TClonesArray.h"
#include "TFile.h"
#include "TSystem.h"

#include "Randomize.hh"
#include <iomanip>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMEventAction::EDMEventAction(EDMHistoManager *histo)
  : G4UserEventAction()
{

  fPrintModulo = 10000;
  
  fRunAction = (EDMRunAction*)G4RunManager::GetRunManager()->GetUserRunAction();
  fHisto = histo;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMEventAction::~EDMEventAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMEventAction::BeginOfEventAction(const G4Event* evt)
{
  
  G4int evtNb = evt->GetEventID();
  if (evtNb%fPrintModulo == 0) { 
    G4cout << "\n---> Begin of event: " << evtNb << G4endl;
    CLHEP::HepRandom::showEngineStatus();
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMEventAction::EndOfEventAction(const G4Event* evt)
{

  G4int hcID = G4SDManager::GetSDMpointer()->GetCollectionID("TPCHitCollection");
  G4VHitsCollection* hc = evt->GetHCofThisEvent()->GetHC(hcID);

  std::vector<EDMTPCHit*> hitsVector;

  for(G4int ih=0;ih<hc->GetSize();ih++){

    EDMTPCHit *aHit = (EDMTPCHit*)hc->GetHit(ih);
    hitsVector.push_back(aHit);
    
  }
  
  //Fill ntuple
  fHisto->FillNtuple(evt->GetEventID(),&hitsVector,fKine);
  
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
