#include "EDMDetectorConstruction.hh"
#include "EDMDetectorMessenger.hh"
#include "EDMRTPCSD.hh"
#include "EDMLTPCSD.hh"
#include "EDMMagneticField.hh"

#include "G4UserLimits.hh"
#include "G4Material.hh"
#include "G4SDManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "globals.hh"
//#include "G4GDMLParser.hh"

using namespace CLHEP;
using namespace std;

EDMDetectorConstruction::EDMDetectorConstruction()
{

  fQuencher = "isobutane";
  fFraction = 10.;
  fPressure = 1.*bar;
  
  fDetectorMessenger = new EDMDetectorMessenger(this);

}

EDMDetectorConstruction::~EDMDetectorConstruction()
{

  delete fDetectorMessenger;

}

G4VPhysicalVolume* EDMDetectorConstruction::Construct()
{

  //------------------------------------------------------ materials
  G4NistManager* man = G4NistManager::Instance();

  G4Element* elH  = new G4Element("Hydrogen","H",1,1.0079*g/mole);
  G4Element* elHe  = new G4Element("Helium","He",2.,4.0026*g/mole);
  G4Element* elC  = new G4Element("Carbon","C",6.,12.0107*g/mole);
  G4Element* elO  = new G4Element("Oxygen","O",8.,15.9994*g/mole);
  G4Element* elN  = new G4Element("Nitrogen","N",7.,14.00674*g/mole);
  G4Element* elSi = new G4Element("Silicon", "Si",14.,28.0855*g/mole);
  G4Element* elF = new G4Element("Fluorine", "F",9.,18.998403*g/mole);
  
  G4Material* Vacuum = new G4Material("Vacuum",universe_mean_density,2);
  Vacuum->AddElement(elN,0.7);
  Vacuum->AddElement(elO,0.3);
  
  G4Material* He =
    new G4Material("HeliumGas",0.0001786*g/cm3,1);
  He->AddElement(elHe,1);

  G4Material* CO2 = 
    new G4Material("CO2",0.001977*g/cm3,2);
  CO2->AddElement(elC,1);
  CO2->AddElement(elO,2);

  G4Material* CF4 = 
    new G4Material("CF4",0.00372*g/cm3,2);
  CF4->AddElement(elC,1);
  CF4->AddElement(elF,4);

  G4Material* C4H10 = 
    new G4Material("C4H10",0.00251*g/cm3,2);
  C4H10->AddElement(elC,4);
  C4H10->AddElement(elH,10);

  G4Material *quenchGas = 0;
  
  if(fQuencher=="isobutane") quenchGas = C4H10;
  else if(fQuencher=="co2") quenchGas = CO2;
  else if(fQuencher=="cf4") quenchGas = CF4;

  G4double std_density = (1-fFraction/100.)*He->GetDensity() + fFraction/100.*quenchGas->GetDensity();
  G4double temperature = 293.15*kelvin;
  G4Material* TPCgas = new G4Material("TPCgas",fPressure/(1.*bar)*std_density,2,kStateGas, temperature, fPressure);
  TPCgas->AddMaterial(He,(1-fFraction/100.)*He->GetDensity()/std_density);
  TPCgas->AddMaterial(quenchGas,fFraction/100.*quenchGas->GetDensity()/std_density);

  G4cout << "##############################################" << G4endl;
  G4cout << "Gas settings: " << G4endl;
  G4cout << TPCgas << G4endl;
  G4cout << "##############################################" << G4endl;
  
    
  /*
  G4Material* He_CO2_90_10 =
    new G4Material("He_CO2_90_10",0.00035844*g/cm3,2);
  He_CO2_90_10->AddElement(elHe,0.4484);
  He_CO2_90_10->AddMaterial(CO2,0.5516);

  G4Material* He_C4H10_90_10 =
    new G4Material("He_C4H10_90_10",0.0004094*g/cm3,2); // 0.5 bar
  He_C4H10_90_10->AddElement(elHe,0.3869);
  He_C4H10_90_10->AddMaterial(C4H10,0.6131);
  */
  
  G4Material* Si3N4 = 
    new G4Material("Si3N4",3.17*g/cm3,2);
  Si3N4->AddElement(elSi,3);
  Si3N4->AddElement(elN,4);

  G4Material *PEEK = new G4Material("PEEK",1.31*g/cm3,3);
  PEEK->AddElement(elC,0.76);
  PEEK->AddElement(elH,0.08);
  PEEK->AddElement(elO,0.16);

  G4Material* Beryllium = man->FindOrBuildMaterial("G4_Be");

  G4Material* Mylar = man->FindOrBuildMaterial("G4_MYLAR");
  G4Material* Kapton = man->FindOrBuildMaterial("G4_KAPTON");
  G4Material* Silicon = man->FindOrBuildMaterial("G4_Si");
  G4Material* Polyethylene = man->FindOrBuildMaterial("G4_POLYETHYLENE");
  G4Material* Scintillator = man->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
  G4Material* Iron = man->FindOrBuildMaterial("G4_Fe");
  
  ///Experimental hall
  G4Box* experimentalHall_box
    = new G4Box("expHall_box",1.*m,1.*m,1*m);
  G4LogicalVolume* experimentalHall_log = new G4LogicalVolume(experimentalHall_box,
							      Vacuum,"expHall_log",0,0,0);
  G4VPhysicalVolume* experimentalHall_phys = new G4PVPlacement(0,G4ThreeVector(),
							       experimentalHall_log,"expHall",0,false,0);

  G4double BEN_Rin = 54.*mm;  
  G4double BEN_thick = 10.*mm;
  G4double BEN_length = 1.*m;
  
  G4Tubs* magnet_tube = new G4Tubs("magnet_tube",BEN_Rin,BEN_Rin+BEN_thick,BEN_length/2., 0.*deg,360.*deg);

  G4LogicalVolume* magnet_log = new G4LogicalVolume(magnet_tube,Iron,"magnet_log",0,0,0);

  G4VPhysicalVolume* magnet_phys = new G4PVPlacement(0,
  						     G4ThreeVector(0.,0.,0.),
  						     magnet_log,"magnet",experimentalHall_log,false,0);

  //TO BE PUT IN MESSENGER
  
  G4double stepLimit = 1.*mm;
  
  G4ThreeVector cross1(-26.01469*mm,26.030700*mm,240.08900*mm);
  G4ThreeVector cross2(-26.01469*mm,-26.030700*mm,240.08900*mm);
  G4ThreeVector Pcross1(-16.65159*MeV,-17.52070*MeV,-14.13329*MeV);
  G4ThreeVector Pcross2(-16.65159*MeV,17.52070*MeV,-14.13329*MeV);

  G4ThreeVector ZAXIS(0.,0.,1.);
  G4ThreeVector XAXIS(1.,0.,0.);
  G4ThreeVector YAXIS(0.,1.,0.);
  double anglecross = Pcross1.angle(ZAXIS);

  G4double cath_Rin = 44.*mm;  
  G4double cath_thick = 10.*mm;
  G4double cath_length = 70.*mm;

  G4double anode_Rout = 28.*mm;
  G4double anode_thick = 2.*mm;
  G4double anode_length = 70.*mm;

  G4double cap_thick = 10.*mm;

  G4double channel_Rin = 28.*mm; //internal inner radius of the entrance channel
  G4double channel_Rout = cath_Rin; //internal outer radius of the entrance channel
  G4double channel_wall = 1.5*mm;
  G4double channel_thick = 15.*mm;
  
  G4double sub_side = 100.*mm; //side of the auxiliary box used to cut the entrance channel

  G4double window_rside = channel_Rout-channel_Rin+2*channel_wall;
  G4double window_zside = channel_thick/sin(anglecross); 
  G4double window_thick = 0.3*micrometer; 

  ///////////////////
  
  G4RotationMatrix rot1;
  rot1.rotateZ(45.*deg);
  rot1.rotateX(pi-anglecross);  
  G4ThreeVector trans1 = cross1.perpPart() - sub_side/2.*Pcross1.unit();

  G4RotationMatrix rot11;
  rot11.rotateZ(-45.*deg);
  G4ThreeVector newX = rot11*XAXIS;
  rot11.rotate(anglecross,newX);
  G4ThreeVector trans11 = cross1.perpPart() - window_thick/2.*Pcross1.unit() + (cath_length/2.+cap_thick+channel_thick/2.)*ZAXIS;
  
  anglecross = Pcross2.angle(ZAXIS);
  G4RotationMatrix rot2;
  rot2.rotateZ(-45.*deg);
  rot2.rotateX(anglecross);  
  G4ThreeVector trans2 = cross2.perpPart() - sub_side/2.*Pcross2.unit();
  
  G4RotationMatrix rot21;
  rot21.rotateZ(45.*deg);
  newX = rot21*XAXIS;
  rot21.rotate(-anglecross,newX);
  G4ThreeVector trans21 = cross2.perpPart() - window_thick/2.*Pcross2.unit() + (cath_length/2.+cap_thick+channel_thick/2.)*ZAXIS;

  G4ThreeVector tpc_center(0.,0.,cross1.z()-cath_length/2.-cap_thick-channel_thick/2.);

  /////Virtual TPC volume
  G4Tubs* tpc_tube = new G4Tubs("tpc_tube",0.,cath_Rin+cath_thick,0.5*cath_length+cap_thick+channel_thick+10*mm, 0.*deg,360.*deg);
  G4LogicalVolume* tpc_log = new G4LogicalVolume(tpc_tube,Vacuum,"tpc_log",0,0,0);
  G4VPhysicalVolume* tpc_phys = new G4PVPlacement(0,tpc_center,tpc_log,"tpc",experimentalHall_log,false,0);

  /////Gas volume
  G4Tubs* gas_tube = new G4Tubs("tpc_tube",0.,cath_Rin+cath_thick,(cath_length+2*cap_thick)/2., 0.*deg,360.*deg);
  G4LogicalVolume* gas_log = new G4LogicalVolume(gas_tube,TPCgas,"gas_log",0,0,0);
  G4VPhysicalVolume* gas_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),gas_log,"gas",tpc_log,false,0);

  /////Cathode
  G4Tubs* cath_tube = new G4Tubs("cath_tube",cath_Rin,cath_Rin+cath_thick,cath_length/2., 0.*deg,360.*deg);
  G4LogicalVolume* cath_log = new G4LogicalVolume(cath_tube,PEEK,"cath_log",0,0,0);
  G4VPhysicalVolume* cath_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),cath_log,"cathode",gas_log,false,0);

  /////Anode
  G4Tubs* anode_tube = new G4Tubs("anode_tube",anode_Rout-anode_thick,anode_Rout,anode_length/2., 0.*deg,360.*deg);
  G4LogicalVolume* anode_log = new G4LogicalVolume(anode_tube,PEEK,"anode_log",0,0,0);
  G4VPhysicalVolume* anode_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),anode_log,"anode",gas_log,false,0);

  /////End caps
  G4Tubs* cap_tube = new G4Tubs("cap_tube",0.,cath_Rin+cath_thick,cap_thick/2., 0.*deg,360.*deg);
  G4Tubs* cap_hole = new G4Tubs("cap_hole",channel_Rin,channel_Rout,cap_thick/2.+0.001*mm, 135.*deg,90.*deg);
  G4VSolid* cap_channel = new G4SubtractionSolid("cap_channel",cap_tube,cap_hole, 0, G4ThreeVector(0.,0.,0.));

  G4LogicalVolume* cap_log = new G4LogicalVolume(cap_tube,PEEK,"cap_log",0,0,0);
  G4LogicalVolume* cap_channel_log = new G4LogicalVolume(cap_channel,PEEK,"cap_channel_log",0,0,0);

  G4VPhysicalVolume* cap_phys1 = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.5*cath_length+0.5*cap_thick),
  						   cap_channel_log,"cap_in",gas_log,false,0);
  
  G4VPhysicalVolume* cap_phys2 = new G4PVPlacement(0,G4ThreeVector(0.,0.,-(0.5*cath_length+0.5*cap_thick)),
  						   cap_log,"cap_out",gas_log,false,0);
  
  /////Entrance channels
  G4Tubs* channel_tube_full = new G4Tubs("channel_tube_full",channel_Rin-channel_wall,channel_Rout+channel_wall,channel_thick/2.,90.*deg,180.*deg);
  G4Box* channel_box = new G4Box("channel_box",sub_side/2.,sub_side/2.,sub_side/2.);
  G4VSolid* channel_tube_cut = new G4SubtractionSolid("channel_tube_cut",channel_tube_full,channel_box, &rot1, trans1);
  G4VSolid* channel_tube = new G4SubtractionSolid("channel_tube",channel_tube_cut,channel_box, &rot2, trans2);

  G4LogicalVolume* channel_log = new G4LogicalVolume(channel_tube,PEEK,"channel_log",0,0,0);
  
  G4Tubs* channel_inner_tube_full = new G4Tubs("channel_inner_tube_full",channel_Rin,channel_Rout,(channel_thick-channel_wall)/2.,90.*deg,180.*deg);
  G4VSolid* channel_inner_tube_cut = new G4SubtractionSolid("channel_inner_tube_cut",channel_inner_tube_full,channel_box, &rot1, trans1+0.5*channel_wall*ZAXIS);
  G4VSolid* channel_inner_tube = new G4SubtractionSolid("channel_inner_tube",channel_inner_tube_cut,channel_box, &rot2, trans2+0.5*channel_wall*ZAXIS);

  G4VPhysicalVolume* channel_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.5*cath_length+cap_thick+0.5*channel_thick),
						      channel_log,"channel",tpc_log,false,0);

  G4LogicalVolume* channel_inner_log = new G4LogicalVolume(channel_inner_tube,TPCgas,"channel_inner_log",0,0,0);  
  G4VPhysicalVolume* channel_inner_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,-0.5*channel_wall),
							    channel_inner_log,"channel_inner",channel_log,false,0);

  /////Entrance Window
  G4Box* window_box = new G4Box("window_box",window_rside/2.,window_zside/2.,window_thick/2.);
  G4LogicalVolume* window_log = new G4LogicalVolume(window_box,Si3N4,"window_log",0,0,0);
  G4VPhysicalVolume* window_minus = new G4PVPlacement(G4Transform3D(rot11,trans11),window_log,"window",tpc_log,false,0);
  G4VPhysicalVolume* window_plus = new G4PVPlacement(G4Transform3D(rot21,trans21),window_log,"window",tpc_log,false,0);

  /////Sensitive volume
  G4Tubs* active_tpc_tube = new G4Tubs("active_tpc_tube",anode_Rout,cath_Rin,anode_length/2., 0.*deg,360.*deg);
  G4LogicalVolume* active_tpc_log = new G4LogicalVolume(active_tpc_tube,TPCgas,"active_tpc_log",0,0,0);
  G4VPhysicalVolume* active_tpc_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),active_tpc_log,"active_tpc",gas_log,false,0);

  active_tpc_log->SetUserLimits(new G4UserLimits(stepLimit));
  
  EDMMagneticField *MagneticField = new EDMMagneticField();
  MagneticField->ReadField();

  //G4UniformMagField* MagneticField = new G4UniformMagField(G4ThreeVector(0.,0.,3.*tesla));
  
  G4FieldManager* fieldMgr
    = G4TransportationManager::GetTransportationManager()->GetFieldManager();

  fieldMgr->SetDetectorField(MagneticField);
  fieldMgr->CreateChordFinder(MagneticField);
  
  G4String trackerChamberSDname = "EDM/tpc_SD";
  EDMRTPCSD* TPC_SD = new EDMRTPCSD(trackerChamberSDname,"TPCHitCollection");
  //EDMLTPCSD* TPC_SD = new EDMLTPCSD(trackerChamberSDname,"TPCHitCollection");
  G4SDManager::GetSDMpointer()->AddNewDetector(TPC_SD);

  TPC_SD->SetGas(fQuencher,fFraction,fPressure);
  
  SetSensitiveDetector("active_tpc_log",TPC_SD,true);
  
  return experimentalHall_phys;

}

