#include "EDMSteppingAction.hh"
#include "EDMEventAction.hh"
#include "EDMDetectorConstruction.hh"

#include "G4ParticleTable.hh"
#include "G4Step.hh"
#include "G4RunManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

using namespace std;
using namespace CLHEP;

EDMSteppingAction::EDMSteppingAction()					 
{

  fParticleTable = G4ParticleTable::GetParticleTable();
  
  fDetector = (EDMDetectorConstruction*)
             G4RunManager::GetRunManager()->GetUserDetectorConstruction();
  fEventAction = (EDMEventAction*)
                G4RunManager::GetRunManager()->GetUserEventAction();	       
 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMSteppingAction::~EDMSteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EDMSteppingAction::UserSteppingAction(const G4Step* aStep)
{

  G4Track* track = aStep->GetTrack();

  G4ThreeVector mom = track->GetMomentum();
  G4ThreeVector pos = track->GetPosition();

  G4ThreeVector mom_vtx = track->GetVertexMomentumDirection()*(track->GetVertexKineticEnergy() + track->GetDefinition()->GetPDGMass());
  G4ThreeVector vtx = track->GetVertexPosition();

  G4int parentID = track->GetParentID();

  // get volume of the current step
  //G4VPhysicalVolume* volume  
  //  = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume();

  G4VPhysicalVolume* volume = track->GetNextVolume();
  const G4VProcess* process = aStep->GetPostStepPoint()->GetProcessDefinedStep();

  /*
  if(
     //volume == fDetector->GetTarget() &&
     process->GetProcessName() == "Decay" &&
     mom.mag() == 0.*keV &&
     track->GetDefinition() == fParticleTable->FindParticle("mu+")
     ){

    fEventAction->SetVtx(pos);

  }
  */
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
