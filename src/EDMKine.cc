#include "EDMKine.hh"

EDMKine::EDMKine()
{

  u = 0.;
  v = 0.;
  uprime = 0.;
  vprime = 0.;

  xvtx = 0.;
  yvtx = 0.;
  zvtx = 0.;

  xmom = 0.;
  ymom = 0.;
  zmom = 0.;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMKine::~EDMKine() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EDMKine::EDMKine(const EDMKine& right)
{

  u   = right.u;
  v   = right.v;
  uprime   = right.uprime;
  vprime   = right.vprime;

  xvtx   = right.xvtx;
  yvtx   = right.yvtx;
  zvtx   = right.zvtx;

  xmom   = right.xmom;
  ymom   = right.ymom;
  zmom   = right.zmom;

}


const EDMKine& EDMKine::operator=(const EDMKine& right)
{

  u   = right.u;
  v   = right.v;
  uprime   = right.uprime;
  vprime   = right.vprime;

  xvtx   = right.xvtx;
  yvtx   = right.yvtx;
  zvtx   = right.zvtx;

  xmom   = right.xmom;
  ymom   = right.ymom;
  zmom   = right.zmom;

  return *this;
}

void EDMKine::SetPhaseSpace(G4double uu, G4double uuprime, G4double vv, G4double vvprime)
{

  u = uu;
  v = vv;
  uprime = uuprime;
  vprime = vvprime;

}
  
void EDMKine::SetMomentum(G4double px, G4double py, G4double pz)
{

  xmom = px;
  ymom = py;
  zmom = pz;

}

void EDMKine::SetVertex(G4double x, G4double y, G4double z)
{

  xvtx = x;
  yvtx = y;
  zvtx = z;

}



