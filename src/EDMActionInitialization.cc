#include "EDMActionInitialization.hh"
#include "EDMPrimaryGeneratorAction.hh"

EDMActionInitialization::EDMActionInitialization()
 : G4VUserActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......                                        

EDMActionInitialization::~EDMActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......                                        

void EDMActionInitialization::BuildForMaster() const
{
  //SetUserAction(new EDMRunAction);
}

void EDMActionInitialization::Build() const
{
  //SetUserAction(new EDMPrimaryGeneratorAction);
}



