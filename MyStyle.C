void MyStyle(){

  //Int_t nfont = 42;
  Int_t nfont = 132;

  gStyle->SetLineColor(kBlack);
  
  gStyle->SetPalette(kBird);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetFrameFillColor(0);
  //gStyle->SetTitleColor(0); 
                                    
  gStyle->SetStatColor(0);
  gStyle->SetTitleFillColor(0);

  // set the paper & margin sizes
                                    
  gStyle->SetPaperSize(20,26);
  gStyle->SetPadTopMargin(0.05);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.18);

  // use large Times-Roman fonts 
                                    
  gStyle->SetTitleFont(nfont,"xyz");  // set the all 3 axes title font
  gStyle->SetTitleFont(nfont," ");    // set the pad title font
  gStyle->SetTitleSize(0.07,"xyz"); // set the 3 axes title size
  gStyle->SetTitleSize(0.07," ");   // set the pad title size
                                    
  gStyle->SetLabelFont(nfont,"xyz");
  gStyle->SetLabelSize(0.06,"xyz");
  gStyle->SetTextFont(nfont);
  gStyle->SetTextSize(0.1);
  gStyle->SetStatFont(nfont);

  // use bold lines and markers
                                    
  gStyle->SetMarkerStyle(8);
  gStyle->SetMarkerSize(1.);
  gStyle->SetHistLineWidth((Width_t)2.);
  gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
                                   

  //..Get rid of X error bars 
  
  //gStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations 

  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);

  // put tick marks on top and RHS of plots
                                    
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  gStyle->SetLegendBorderSize(0);

  gStyle->SetNdivisions(505,"XY");

}
