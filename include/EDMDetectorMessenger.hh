#ifndef EDMDetectorMessenger_h
#define EDMDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class EDMDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EDMDetectorMessenger: public G4UImessenger
{
  public:
    EDMDetectorMessenger(EDMDetectorConstruction* );
    virtual ~EDMDetectorMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
private:
  
  EDMDetectorConstruction* fDetector;
  
  G4UIdirectory*             fEDMDir;
  G4UIdirectory*             fDetDir;
  G4UIcmdWithAString* fQuencherCmd;
  G4UIcmdWithADouble* fFractionCmd;
  G4UIcmdWithADoubleAndUnit* fPressureCmd;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

