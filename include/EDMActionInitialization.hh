#ifndef EDMActionInitialization_h
#define EDMActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

/// Action initialization class.                                                                                        
///                                                                                                                     

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......                                        

class EDMActionInitialization : public G4VUserActionInitialization
{
  public:
    EDMActionInitialization();
    virtual ~EDMActionInitialization();

    virtual void BuildForMaster() const;

  virtual void Build() const;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......                                        
#endif
