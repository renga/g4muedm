//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EDMRTPCSD.hh
/// \brief Definition of the EDMRTPCSD class

#ifndef EDMRTPCSD_h
#define EDMRTPCSD_h 1

#include "G4VSensitiveDetector.hh"

#include "EDMTPCHit.hh"

#include "TH2D.h"

#include <vector>

class G4Step;
class G4HCofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/// EDMRTPC sensitive detector class
///
/// The hits are accounted in hits in ProcessHits() function which is called
/// by Geant4 kernel at each step. A hit is created with each step with non zero 
/// energy deposit.

class EDMRTPCSD : public G4VSensitiveDetector
{

public:
  
  EDMRTPCSD(const G4String& name, 
	    const G4String& hitsCollectionName);
  virtual ~EDMRTPCSD();
  
  // methods from base class
  virtual void   Initialize(G4HCofThisEvent* hitCollection);
  virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);
  virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);
  
  void SetGas(G4String quencher, G4double fraction, G4double pressure);

private:
  
  EDMTPCHitsCollection* fHitsCollection;
  void SpecificIonizationElectron(G4double _pMom, G4double &nhe, G4double &niso);
  void ReadGasDatabase();

  //G4double lambda_mu;
  //G4double ave_nele; 
  G4double fvdrift;
  G4double fDT;
  G4double fDL;
  G4double fNclu;
  G4double fNe;

  TH2D *hDT[3];
  TH2D *hDL[3];
  TH2D *hvdrift[3];
  TH2D *hNclu[3];
  TH2D *hNe[3];
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
