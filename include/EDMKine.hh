#ifndef EDMKine_h
#define EDMKine_h 1

#include "G4Types.hh"

class EDMKine
{
  
public:
  
  EDMKine();
  EDMKine(const EDMKine&);
  virtual ~EDMKine();

  const EDMKine& operator=(const EDMKine&);

  // Set methods
  void SetPhaseSpace(G4double uu, G4double uuprime, G4double vv, G4double vvprime);
  void SetMomentum(G4double px, G4double py, G4double pz);
  void SetVertex(G4double x, G4double y, G4double z);
  
  G4double u;
  G4double v;
  G4double uprime;
  G4double vprime;

  G4double xvtx;
  G4double yvtx;
  G4double zvtx;

  G4double xmom;
  G4double ymom;
  G4double zmom;

};

#endif
