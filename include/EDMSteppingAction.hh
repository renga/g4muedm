#ifndef EDMSteppingAction_h
#define EDMSteppingAction_h 1

#include "G4UserSteppingAction.hh"

class EDMDetectorConstruction;
class EDMEventAction;
class G4ParticleTable;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EDMSteppingAction : public G4UserSteppingAction
{
public:
  EDMSteppingAction();
  virtual ~EDMSteppingAction();

  void UserSteppingAction(const G4Step*);
    
private:
  EDMDetectorConstruction* fDetector;
  EDMEventAction* fEventAction;  
  G4ParticleTable* fParticleTable;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
