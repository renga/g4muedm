#ifndef EDMRunAction_h
#define EDMRunAction_h 1

#include "EDMHistoManager.hh"

#include "G4UserRunAction.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Run;

class EDMRunAction : public G4UserRunAction
{
public:
  EDMRunAction(EDMHistoManager* histo);
  virtual ~EDMRunAction();

  void BeginOfRunAction(const G4Run*);
  void EndOfRunAction(const G4Run*);
    
private:

  EDMHistoManager* fHisto;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

