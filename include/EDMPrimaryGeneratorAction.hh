#ifndef EDMPrimaryGeneratorAction_h
#define EDMPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"

#include "TMatrixD.h"

class G4ParticleGun;
class G4Event;
class EDMEventAction;

class EDMPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{

public:
  EDMPrimaryGeneratorAction(EDMEventAction *EventAction);
  ~EDMPrimaryGeneratorAction();
  
public:
  void GeneratePrimaries(G4Event* anEvent);

private:
  G4ParticleGun* particleGun;
  EDMEventAction* fEventAction;
  
  TMatrixD fMphaseU;
  TMatrixD fSphaseU;
  TMatrixD fMphaseV;
  TMatrixD fSphaseV;

  G4ThreeVector fMom;
  G4ThreeVector fVtx;
  
  G4ThreeVector fUaxis;
  G4ThreeVector fVaxis;
  G4ThreeVector fWaxis;

  G4ThreeVector LocalToGlobal(G4ThreeVector uvw);  
  void GeneratePhaseSpace(G4ThreeVector &mom, G4ThreeVector &vtx);  
  
};

#endif


