#ifndef EDMDetectorConstruction_h
#define EDMDetectorConstruction_h 1

class G4LogicalVolume;
class G4VPhysicalVolume;
class EDMDetectorMessenger;

#include "G4VUserDetectorConstruction.hh"

class EDMDetectorConstruction : public G4VUserDetectorConstruction
{
public:
  
  EDMDetectorConstruction();
  ~EDMDetectorConstruction();
  
  G4VPhysicalVolume* Construct();

  void SetQuencher(G4String quencher) { fQuencher = quencher; }
  void SetFraction(G4double fraction) { fFraction = fraction; }
  void SetPressure(G4double pressure) { fPressure = pressure; }

private:

  EDMDetectorMessenger* fDetectorMessenger;
  
  G4String fQuencher;
  G4double fFraction;
  G4double fPressure;
  
};

#endif

