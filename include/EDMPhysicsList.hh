#ifndef EDMPhysicsList_h
#define EDMPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EDMPhysicsList: public G4VUserPhysicsList
{
public:
  EDMPhysicsList();
  virtual ~EDMPhysicsList();

  // Construct particle and physics
  void ConstructParticle();
  void ConstructProcess();
 
  void SetCuts();
   
private:

  // these methods Construct physics processes and register them
  void ConstructDecay();
  void ConstructEM();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif



