#ifndef EDMKine_h
#define EDMKine_h 1

class EDMKine 
{
  
public:
  
  EDMKine();
  EDMKine(const EDMTPCHit&);
  virtual ~EDMKine();
  
  // Set methods
  void SetPhaseSpace(G4double uu, G4double uuprime, G4double vv, G4double vvprime);
  void SetMomentum(G4double vx, G4double vy, G4double vz);
  void SetVertex(G4double x, G4double y, G4double z);
  
private:
  
  G4double u;
  G4double v;
  G4double uprime;
  G4double vprime;

  G4double xvtx;
  G4double yvtx;
  G4double zvtx;

  G4double xmom;
  G4double ymom;
  G4double zmom;

};

#endif
