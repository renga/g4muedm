#ifndef EDMEventAction_h
#define EDMEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

#include "EDMKine.hh"

#include "G4ThreeVector.hh"

class EDMRunAction;
class EDMHistoManager;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EDMEventAction : public G4UserEventAction
{
public:

  EDMEventAction(EDMHistoManager* histo);
  virtual ~EDMEventAction();

  virtual void  BeginOfEventAction(const G4Event*);
  virtual void  EndOfEventAction(const G4Event*);

  void SetPrintModulo(G4int    val)  {fPrintModulo = val;};

  void SetKine(EDMKine kine) { fKine = kine; }
  
private:

  EDMRunAction*  fRunAction;
  EDMHistoManager *fHisto;

  G4int fPrintModulo;
  EDMKine fKine;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
