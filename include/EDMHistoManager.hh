#ifndef EDMHistoManager_h
#define EDMHistoManager_h 1

#include "globals.hh"

#include "G4ThreeVector.hh"

#include "EDMTPCHit.hh"
#include "EDMKine.hh"

#include "TClonesArray.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class TFile;
class TTree;
class TH1D;

using namespace std;

const G4int MaxHisto = 5;

const G4int MaxHits = 1000;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EDMHistoManager
{

public:
  
  EDMHistoManager(G4String outfile);
  ~EDMHistoManager();
  
  void book();
  void save();
  
  void FillHisto(G4int id, G4double bin, G4double weight = 1.0);
  void Normalize(G4int id, G4double fac);    

  void FillNtuple(G4int event, vector<EDMTPCHit*> *TPCHitArr, EDMKine kine);

  void PrintStatistic();
  
private:
  
  TFile*   fRootFile;
  TH1D*    fHisto[MaxHisto];            
  TTree*   fNtuple;

  G4String fOutfile;
  
  G4int fEvent;
  EDMKine fKine;
  vector<EDMTPCHit*> fTPCHitArr;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

