//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EDMTPCHit.hh
/// \brief Definition of the EDMTPCHit class

#ifndef EDMTPCHit_h
#define EDMTPCHit_h 1

#include "TObject.h"

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"

/// Tracker hit class
///
/// It defines data members to store the trackID, chamberNb, energy deposit,
/// and position of charged particles in a selected volume:
/// - fTrackID, fChamberNB, fEdep, fPos

class EDMTPCHit : /*public TObject,*/ public G4VHit
{
  public:
    EDMTPCHit();
    EDMTPCHit(const EDMTPCHit&);
    virtual ~EDMTPCHit();

    // operators
    const EDMTPCHit& operator=(const EDMTPCHit&);
    G4bool operator==(const EDMTPCHit&) const;

    inline void* operator new(size_t);
    inline void  operator delete(void*);

    // methods from base class
    virtual void Draw();
    virtual void Print();

    // Set methods
    void SetTrackID  (G4int track)      { fTrackID = track; };
    void SetEdep     (G4double de)      { fEdep = de; };
    void SetPos      (G4ThreeVector xyz){ fPos = xyz; };
    void SetMom      (G4ThreeVector xyz){ fMom = xyz; };
    void SetChip     (G4int chip)       { fChip = chip; };
    void SetPixelX   (G4int px)         { fPixelX = px; };
    void SetPixelY   (G4int py)         { fPixelY = py; };
    void SetTime     (G4double time)    { fTime = time; };

    // Get methods
    G4int GetTrackID() const     { return fTrackID; };
    G4double GetEdep() const     { return fEdep; };
    G4ThreeVector GetPos() const { return fPos; };
    G4ThreeVector GetMom() const { return fMom; };
    G4int GetChip() const        { return fChip; };
    G4int GetPixelX() const      { return fPixelX; };
    G4int GetPixelY() const      { return fPixelY; };
    G4double GetTime() const     { return fTime; };

  private:

      G4int         fTrackID;
      G4double      fEdep;
      G4ThreeVector fPos;
      G4ThreeVector fMom;
      G4int         fChip;
      G4int         fPixelX;
      G4int         fPixelY;
      G4double      fTime;

  //public:
  //ClassDef(EDMTPCHit,0) 
     

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<EDMTPCHit> EDMTPCHitsCollection;

extern G4ThreadLocal G4Allocator<EDMTPCHit>* EDMTPCHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* EDMTPCHit::operator new(size_t)
{
  if(!EDMTPCHitAllocator)
      EDMTPCHitAllocator = new G4Allocator<EDMTPCHit>;
  return (void *) EDMTPCHitAllocator->MallocSingle();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void EDMTPCHit::operator delete(void *hit)
{
  EDMTPCHitAllocator->FreeSingle((EDMTPCHit*) hit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
